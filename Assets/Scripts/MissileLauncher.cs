﻿//
//
// MissileLauncher  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class MissileLauncher : MonoBehaviour 
{

    #region Public Variables
	
    public Transform            launchSpot;
    public Transform            marker;
    public GameObject           missilePrefab;
    public bool                 CanShoot;
    public bool                 enablePlayerControl = true;

    [Range ( 0.2f , 2.0f )]
    public float                missileSpeed = 0.8f;

    public AudioClip            shootClip1;
    public AudioClip            shootClip2;

    #endregion    

    #region Delegates

    public delegate void OnMissileLauncherEvent( int payload );

    public event OnMissileLauncherEvent     OnShoot;
    public event Missile.OnMissileEvent     OnMissileBlast;

    #endregion

    #region Private Variables

    private Plane           plane;

    #endregion
	
    #region MonoBehavior Functions

    void Awake()
    {
		plane = new Plane( Vector3.forward , transform.position );
    }

	void Update()
	{
        if( !CanShoot )
            return;

        if( !enablePlayerControl )
            return;

        if( IsGamePaused() )
            return;

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float hit;

        if ( plane.Raycast( ray , out hit ) )
        {
            Vector3 aimPosition = ray.GetPoint(hit) - transform.position;
            var aimDirection = aimPosition.normalized;

            Debug.DrawRay(transform.position, aimPosition , Color.red);
            Debug.DrawRay(transform.position, aimDirection, Color.grey);

            Vector3 clickPosition = ray.GetPoint( hit );

            SetMarker( clickPosition );

            if ( Input.GetMouseButtonDown(0) ) 
            {
                ShootBeam( clickPosition );
            }
        }
	}

    #endregion

    #region Public Methods

    public void SetMarker( Vector3 position )
    {
        marker.position = position;
    }

    public void ShootBeam( Vector3 target )
    {
        missilePrefab.SetActive( false );
        var missileObject = Instantiate( missilePrefab ) as GameObject;
        Assert.IsNotNull( missileObject );

        var missile = missileObject.GetComponent<Missile>();

        missile.origin          = launchSpot.position;
        missile.destination     = target;
        missile.speed           = missileSpeed;
        missile.OnBlast         += OnMissileBlast;

        missile.gameObject.SetActive( true );

        OnShoot( 1 );

        SoundManager.instance.RandomizeSfx( shootClip1 , shootClip2 );
    }

    #endregion

    #region Private Methods

    bool IsGamePaused()
    {
        return Time.timeScale == 0;
    }

    #endregion
}
