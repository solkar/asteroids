﻿//
//
// Blast  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Collider2D ) )]
public class Blast : MonoBehaviour 
{
    #region Public Variables

    public float                maxRadius = 1.0f;
    public float                minRadius = 0.1f;
    public AudioClip            explosionClip1;
    public AudioClip            explosionClip2;

    #endregion

    #region MonoBehaviour Functions

    void OnEnable()
    {
       StartCoroutine( CustomUpdate() ); 
    }

    void OnDisable()
    {
        StopAllCoroutines();
    }

    #endregion  

    #region Blast Behaviour

    IEnumerator CustomUpdate()
    {
        float radius = minRadius;
        float speed  = 1.0f;

        while( radius < maxRadius )
        {
            radius += Time.deltaTime * speed;

            transform.localScale = new Vector3( radius , radius , 1.0f );

            yield return 0;
        }

        SoundManager.instance.RandomizeSfx( explosionClip1 , explosionClip2 );

        for (int i = 0; i < 3; i++) 
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return 5;

            GetComponent<SpriteRenderer>().color = Color.white;
            yield return 5;
        }

        Destroy( gameObject );
    }

    #endregion
}
