﻿//
//
// AsteroidSpawner  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class AsteroidController : MonoBehaviour 
{
    #region Public Variables

    public GameObject   prefab;

    [Range ( 0.2f , 2.0f )]
    public float asteroidSpeedMax = 1.2f;

    [Range ( 0.2f , 2.0f )]
    public float asteroidSpeedMin = 0.2f;

    [Range ( 6 , 12 )]
    public int skyHeight = 11;

    [Range ( 1 , 8 )]
    public float dispersion = 5;

    [Range ( 1 , 8 )]
    public float coherence = 5;

    #endregion

    #region Delegates

    public delegate void AsteroidControllerEvent( Asteroid a );
    public event AsteroidControllerEvent OnAsteroidSpawn;

    public event Asteroid.AsteroidEvent  OnAsteroidExplosion;

    #endregion

    #region Private properties
    
    private Plane       skyPlane;

    #endregion

    #region Public methods

    public void SpawnAsteroidPack( int packSize )
    {
        for (int i = 0; i < packSize; i++) 
        {
            Vector2 sweetSpot   = GetRandomSweetSpot();
            float speed = Random.Range( asteroidSpeedMin , asteroidSpeedMax );

            SpawnAsteroid( sweetSpot , speed , dispersion );  
            SpawnAsteroid( sweetSpot , speed , -dispersion );  
        }
    }

    public void SpawnSolitaire()
    {
        float speed = Random.Range( asteroidSpeedMin , asteroidSpeedMax );
        float c = Random.Range( - dispersion , dispersion );
        SpawnAsteroid( GetRandomSweetSpot() , speed , c );
    }

    #endregion

    #region MonoBehaviour Functions

    void Awake()
    {
	    skyPlane = new Plane( Vector3.down , new Vector3( 0 , skyHeight , 0 ) );
    }

    #endregion


    #region Private methods

    void SpawnAsteroid( Vector2 sweetSpot , float speed , float dispersion )
    {
        var data = GenerateAsteroid( sweetSpot , dispersion );

        prefab.SetActive( false );
        var asteroidObject = Instantiate( prefab ) as GameObject;
        var asteroid = asteroidObject.GetComponent<Asteroid>();
        Assert.IsNotNull( asteroid );

        asteroid.origin         = data[ 0 ];
        asteroid.destination    = data[ 1 ];
        asteroid.speed          = speed;
        asteroid.OnExplode      += OnAsteroidExplodes;

        asteroid.gameObject.SetActive( true );

        if( OnAsteroidSpawn != null )
            OnAsteroidSpawn( asteroid );
    }

    Vector2[] GenerateAsteroid( Vector2 sweetSpot , float dispersion ) 
    {
        Vector2 destination = new Vector2( dispersion , 0 );

        Vector2 origin = GetOriginAtSkyPlane( destination , sweetSpot ); 

        Debug.Log( "Asteroid target: " + destination );
        Debug.Log( "Asteroid origin: " + origin );

        var data = new Vector2[]
        {
            origin,
            destination,
        };

        return data;
    }

    Vector2 GetOriginAtSkyPlane( Vector2 destination , Vector2 sweetSpot )
    {
        Ray ray = new Ray( destination , ( sweetSpot - destination ).normalized );
        float hit;
        if( skyPlane.Raycast( ray , out hit ) )
        {
           return ( Vector2 ) ray.GetPoint( hit );
        }
        else
        {
            Debug.LogWarning( "No collision with sky plane" );
            return sweetSpot;
        }
    }
    

    void OnAsteroidExplodes()
    {
        //Debug.Log( "asteroid explodes. update score" );
        ScoreData.Points++;

        OnAsteroidExplosion();
    }

    Vector2 GetRandomSweetSpot()
    {
        float x = Random.Range( - coherence , coherence );
        float y = Random.Range(  4 , 7 );

        return new Vector2( x , y );
    }

    #endregion
}
