﻿//
//
// InstantiateOnClick  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class InstantiateOnClick : MonoBehaviour 
{
    #region Public Variables

    public GameObject prefab;

    #endregion

    #region MonoBehaviour Functions

    void Update()
    {
        if( Input.GetMouseButtonDown( 0 ) )
        {
            Vector3 v = Camera.main.ScreenToWorldPoint( Input.mousePosition );
            v.z = 0;
            AddPrefab( v );
        }
    }

    #endregion

    #region Private Methods

    void AddPrefab( Vector3 spawnPosition )
    {
        Instantiate( prefab , spawnPosition , Quaternion.identity );
    }

    #endregion

}
