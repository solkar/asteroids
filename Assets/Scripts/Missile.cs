﻿//
//
// Missile  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class Missile : Asteroid 
{
    #region Public Variables

    public GameObject       blastPrefab;
    public AudioClip        missileClip1;

    #endregion

    #region Delegates

    public delegate void OnMissileEvent();
    public event OnMissileEvent OnBlast;

    #endregion

    #region Missile Behavior

    override protected IEnumerator CustomUpdate()
    {

        yield return StartCoroutine( ReachDestination() );

        //Debug.Log( "Instantiate blast" );
        Instantiate( blastPrefab , transform.position , Quaternion.identity );

        OnBlast();

        SoundManager.instance.PlaySingle( missileClip1 );

        Destroy( gameObject );
    }


    IEnumerator ReachDestination()
    {
        transform.position = origin;

        Vector3 destinationDirection  = destination - origin;
        destinationDirection.Normalize();

        float targetDistance = Vector3.Distance( destination , origin ) ;
        float travelledDistance = 0;

        while( travelledDistance < targetDistance )
        {
            float step = Time.deltaTime * speed;
            travelledDistance += step; 

            Debug.DrawLine( transform.position, destination , Color.red);

            transform.position += destinationDirection * step;
            yield return 0;
        }
    }

    #endregion
}
