﻿//
//
// GameStateMachine  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class GameStateMachine : MonoBehaviour 
{
    #region MonoBehavior Functions

    void Start()
    {
        StartCoroutine( StateMachine() );
    }
     
    #endregion

    #region States

    IEnumerator StateMachine()
    {

        yield return StartCoroutine( PreGame() );

        yield return StartCoroutine( GameLoop() );

        yield return StartCoroutine( GameOver() );

    }

    virtual protected IEnumerator PreGame()
    {
        yield return 0;
    }

    virtual protected IEnumerator GameLoop()
    {

        while( !IsGameFinished() )
        {
            yield return 0;
        }
    }

    virtual protected IEnumerator GameOver()
    {
        Debug.Log( "Game Over!!!" );
        yield return 0;
    }

    #endregion

    #region Helper Methods

    virtual protected bool IsGameFinished()
    {
        return false;
    }

    #endregion

}
