﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;


public class StartOptions : MonoBehaviour
{
    #region Public Variables

	public int      sceneToStart = 1;
    public int      autoSceneIndex = 2;
	public bool     changeScenes;											
	public bool     changeMusicOnStart;									
	public int      musicToChangeTo = 0;							

	[HideInInspector] public bool               inMainMenu = true;					
	[HideInInspector] public Animator           animColorFade; 				
	[HideInInspector] public Animator           animMenuAlpha;			
	[HideInInspector] public AnimationClip      fadeColorAnimationClip;		
	[HideInInspector] public AnimationClip      fadeAlphaAnimationClip;	

    #endregion

    #region Private Variables

	private PlayMusic       playMusic;			
	private float           fastFadeIn = .01f;	
	private ShowPanels      showPanels;	

    #endregion
	
    #region MonoBehavior Functions

	void Awake()
	{
		showPanels  = GetComponent<ShowPanels> ();
		playMusic   = GetComponent<PlayMusic> ();
	}

    #endregion

    #region Handle Menu Controls

	public void StartButtonClicked()
	{
		if (changeMusicOnStart) 
		{
			playMusic.FadeDown(fadeColorAnimationClip.length);
			Invoke ("PlayNewMusic", fadeAlphaAnimationClip.length);
		}

		if (changeScenes) 
		{
			Invoke ("LoadRegularGameDelayed", fadeColorAnimationClip.length * .5f);
			animColorFade.SetTrigger ("fade");
		} 
		else 
		{
			StartGameInScene();
		}
	}

    public void AutoButtonClicked()
    {
		if (changeMusicOnStart) 
		{
			playMusic.FadeDown(fadeColorAnimationClip.length);
			Invoke ("PlayNewMusic", fadeAlphaAnimationClip.length);
		}

		if (changeScenes) 
		{
			Invoke ("LoadAutoGameDelayed", fadeColorAnimationClip.length * .5f);
			animColorFade.SetTrigger ("fade");
		} 
		else 
		{
			StartGameInScene();
		}

    }

    #endregion

    #region Load Game Scene Helpers

	public void LoadRegularGameDelayed()
	{
		inMainMenu = false;

		showPanels.HideMenu ();

		Application.LoadLevel (sceneToStart);
	}

	public void LoadAutoGameDelayed()
	{
		inMainMenu = false;

		showPanels.HideMenu ();

		Application.LoadLevel ( autoSceneIndex );
	}

	public void StartGameInScene()
	{
		inMainMenu = false;

		if (changeMusicOnStart) 
		{
			Invoke ("PlayNewMusic", fadeAlphaAnimationClip.length);
		}

		animMenuAlpha.SetTrigger ("fade");

		Invoke("HideDelayed", fadeAlphaAnimationClip.length);
	}

    #endregion

    #region Helper Methods

	public void PlayNewMusic()
	{
		playMusic.FadeUp (fastFadeIn);
		playMusic.PlaySelectedMusic (musicToChangeTo);
	}

	public void HideDelayed()
	{
		showPanels.HideMenu();
	}

    #endregion
}
