﻿using UnityEngine;
using System.Collections;

public class DontDestroyMenu : MonoBehaviour 
{
    #region MonoBehaviour Functions

    protected  void Awake()
	{
        var objects = GameObject.FindGameObjectsWithTag( "MainMenu" );

        for (int i = 1; i < objects.Length; i++) 
        {
            Destroy( objects[ i ] );
        }
	}

    void Start()
	{
		DontDestroyOnLoad(this.gameObject);
	}

    #endregion
}
