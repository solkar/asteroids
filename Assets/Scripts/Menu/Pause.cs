﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( ShowPanels ) )]
[RequireComponent( typeof( StartOptions ) )]
public class Pause : MonoBehaviour 
{
    #region Private Variables

	private ShowPanels      showPanels;						
	private bool            isPaused;					
	private StartOptions    startScript;			
	
    #endregion

    #region MonoBehavior Functions

	void Awake()
	{
		showPanels  = GetComponent<ShowPanels> ();
		startScript = GetComponent<StartOptions> ();
	}

	void Update () 
    {
		if (Input.GetButtonDown ("Cancel") && !isPaused && !startScript.inMainMenu) 
		{
			DoPause();
		} 
		else if (Input.GetButtonDown ("Cancel") && isPaused && !startScript.inMainMenu) 
		{
			UnPause ();
		}
	}

    #endregion

    #region Public Methods

	public void DoPause()
	{
		isPaused = true;

		Time.timeScale = 0;

		showPanels.ShowPausePanel ();
	}


	public void UnPause()
	{
		isPaused = false;

		Time.timeScale = 1;

		showPanels.HidePausePanel ();
	}

    public void LoadMainMenu()
    {
        UnPause();
        Application.LoadLevel( 0 );
    }

    #endregion
}
