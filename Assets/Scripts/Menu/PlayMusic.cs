﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class PlayMusic : MonoBehaviour 
{
    
    #region Public Variables

	public AudioClip[] musicClips;
	public AudioMixerSnapshot volumeDown;			
	public AudioMixerSnapshot volumeUp;			

    #endregion

    #region Private Variables

	private AudioSource musicSource;		

    #endregion

    #region MonoBehaviour Functions

	void Awake () 
	{
		musicSource = GetComponent<AudioSource> ();
	}

    #endregion
	
    #region Public Methods

	public void PlaySelectedMusic(int musicChoice)
	{
		musicSource.clip = musicClips [musicChoice];

		musicSource.Play ();
	}

	public void FadeUp(float fadeTime)
	{
		volumeUp.TransitionTo (fadeTime);
	}

	public void FadeDown(float fadeTime)
	{
		volumeDown.TransitionTo (fadeTime);
	}

    #endregion
}
