﻿using UnityEngine;
using System.Collections;

public class ShowPanels : MonoBehaviour 
{

    #region Public Variables

	public GameObject optionsPanel;							
	public GameObject optionsTint;						
	public GameObject menuPanel;					
	public GameObject pausePanel;				
	public GameObject gameoverPanel;				

    #endregion

    #region Control Panel Visibility

	public void ShowOptionsPanel()
	{
		optionsPanel.SetActive(true);
		optionsTint.SetActive(true);
	}

	public void HideOptionsPanel()
	{
		optionsPanel.SetActive(false);
		optionsTint.SetActive(false);
	}

	public void ShowMenu()
	{
		menuPanel.SetActive (true);
	}

	public void HideMenu()
	{
		menuPanel.SetActive (false);
	}
	
	public void ShowPausePanel()
	{
		pausePanel.SetActive (true);
		optionsTint.SetActive(true);
	}

	public void HidePausePanel()
	{
		pausePanel.SetActive (false);
		optionsTint.SetActive(false);
	}

	public void ShowGameoverPanel()
	{
		gameoverPanel.SetActive (true);
		optionsTint.SetActive(true);
	}

	public void HideGameoverPanel()
	{
		gameoverPanel.SetActive (false);
		optionsTint.SetActive(false);
	}

    #endregion
}
