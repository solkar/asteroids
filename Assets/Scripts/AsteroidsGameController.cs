﻿//
//
// AsteroidsGameController  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class AsteroidsGameController : GameStateMachine
{
    #region Public Variables

    public CollisionDetector        ground;
    public AudioClip                finalBlastClip1;
    
    #endregion

    #region Private Variables

    private ShowPanels              showPanels;
    private MissileLauncher         gun; 

    #endregion

    #region MonoBehavior Functions

    void Awake()
    {
        var menuObj = GameObject.Find( "MenuUI" ); 
        if( menuObj != null )
        {
            showPanels = menuObj.GetComponent<ShowPanels>(); 
        }

        gun = GameObject.FindGameObjectWithTag( "Gun" ).GetComponent<MissileLauncher>();
        Assert.IsNotNull( gun );
    }

    #endregion

    #region States

    override protected IEnumerator PreGame()
    {
        //Debug.Log( "Reset score" );
        ScoreData.Points = 0;

        yield return 0;
    }

    override protected IEnumerator GameOver()
    {
        gun.CanShoot = false;

        if( showPanels != null )
            showPanels.ShowGameoverPanel();

        SoundManager.instance.PlaySingle( finalBlastClip1 );

        yield return 0;
    }

    override protected bool IsGameFinished()
    {
        return ground.detected;
    }

    #endregion
}
