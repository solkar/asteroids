﻿//
//
// Asteroid  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

public class Asteroid : MonoBehaviour 
{
    #region Public Variables

    public Vector3              origin;
    public Vector3              destination;
    public float                speed = 1.0f;

    public AudioClip            blowClip1;

    #endregion

    #region Delegates

    public delegate void AsteroidEvent();
    public event AsteroidEvent OnExplode;

    #endregion

    #region Private Variables

    private CollisionDetector   detector;

    #endregion

    #region MonoBehaviour Functions

	void OnEnable () 
    {
        detector = GetComponent<CollisionDetector>();
        Assert.IsNotNull( detector );

        StartCoroutine( CustomUpdate() );	
	}
	
    void OnDisable()
    {
        StopAllCoroutines();
    }

    #endregion

    #region Asteroid Behavior

    virtual protected IEnumerator CustomUpdate()
    {
        SetRandomColor();

        transform.position = origin;

        Vector3 destinationDirection  = destination - origin;
        destinationDirection.Normalize();

        float targetDistance = Vector3.Distance( destination , origin ) ;
        float travelledDistance = 0;

        while( travelledDistance < targetDistance )
        {
            if( detector.detected )
            {
                if( OnExplode != null )
                    OnExplode();

                yield return StartCoroutine( Explode() );
                break;
            }

            float step = Time.deltaTime * speed;
            travelledDistance += step; 

            Debug.DrawLine( transform.position, destination , Color.red);

            transform.position += destinationDirection * step;


            yield return 0;
        }

        Destroy( gameObject );
    }

    virtual protected IEnumerator Explode()
    {
        SoundManager.instance.PlaySingle( blowClip1 );

        for (int i = 0; i < 3; i++) 
        {
            GetComponent<SpriteRenderer>().color = Color.red;
            yield return 5;

            GetComponent<SpriteRenderer>().color = Color.white;
            yield return 5;
        }
    }

    IEnumerator MoveToDestination()
    {
        transform.position = origin;

        Vector3 destinationDirection  = destination - origin;
        destinationDirection.Normalize();

        float targetDistance = Vector3.Distance( destination , origin ) ;
        float travelledDistance = 0;

        while( travelledDistance < targetDistance )
        {
            float step = Time.deltaTime * speed;
            travelledDistance += step; 

            Debug.DrawLine( transform.position, destination , Color.red);

            transform.position += destinationDirection * step;
            yield return 0;
        }
    }

    #endregion

    #region Helper Methods

    void SetRandomColor()
    {
        GetComponent<SpriteRenderer>().color = RandomColor();
    }

    Color RandomColor()
    {
        Color32[] c = new Color32[]{
            new Color32( 239 , 233 , 192 , 255 ),
            new Color32( 242 , 200 , 121 , 255 ),
            new Color32( 191 , 144 ,  86 , 255 ),
            new Color32( 140 ,  91 ,  48 , 255 )

        };

         return  (Color) c[ Random.Range( 0 , c.Length ) ];
    }

    #endregion
}

