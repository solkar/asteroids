﻿//
//
// Score  
//
// Created by Karlos Zafra
// 
//


using UnityEngine;
using System.IO;

#if UNITY_EDITOR
using UnityEditor;

[InitializeOnLoad]
#endif
public class ScoreData : ScriptableObject
{
    #region Public Properties

    public static int Points
    {
        get 
        { 
            return Instance._points; 
        }

        set
        {
            Instance._points = value;
        }

    }

    #endregion

    #region Constants

    const string assetName      = "Score";
    const string path           = "Resources";
    const string extension      = ".asset";

    #endregion

    #region Private Variables

    private static ScoreData    instance;

    [SerializeField]
    private int                 _points = 0;

    #endregion


    #region Asset lazy initialization

    static ScoreData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = Resources.Load( assetName ) as ScoreData;

                if (instance == null)
                {
                    instance = CreateInstance<ScoreData>();
#if UNITY_EDITOR
                    string properPath = Path.Combine(Application.dataPath, path );
                    if (!Directory.Exists(properPath))
                    {
                        AssetDatabase.CreateFolder("Assets", "Resources");
                    }

                    string fullPath = Path.Combine( Path.Combine("Assets", path ), assetName + extension );

                    if ( File.Exists( fullPath ) )
                    {
                        instance._points = 0;
                    }
                    else
                    {
                        AssetDatabase.CreateAsset(instance, fullPath);
                    }

#endif
                }
            }

            return instance;
        }
    }

    #endregion

}
