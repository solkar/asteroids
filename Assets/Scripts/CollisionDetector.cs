﻿//
//
// CollisionDetector  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class CollisionDetector : MonoBehaviour 
{
    #region Public Variables

    public string   targetTag;
    public bool     detected = false;

    #endregion

    #region Collider Events

    void OnTriggerEnter2D( Collider2D other )
    {
        if( other.CompareTag( targetTag ) )
        {
            detected = true;
        }
    }

    void OnTriggerExit2D( Collider2D other )
    {
        if( other.CompareTag( targetTag ) )
        {
            detected = false;
        }
    }

    #endregion
}
