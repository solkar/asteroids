﻿//
//
// HudController  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HudController : MonoBehaviour 
{
    #region Public Variables

    public Text         scoreFigure;

    #endregion

    #region MonoBehaviour Functions

    void Update()
    {
        scoreFigure.text = ScoreData.Points.ToString();
    }

    #endregion
}
