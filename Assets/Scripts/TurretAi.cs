//
//
// TurretAi  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

[RequireComponent( typeof( MissileLauncher ) )]
public class TurretAi : MonoBehaviour 
{
    #region Private Variables

    private AsteroidController      asteroidController;
    private MissileLauncher         gun;

    #endregion

    #region MonoBehaviour Functions

    void Awake()
    {
        gun = GetComponent<MissileLauncher>();    

        var controllerObj = GameObject.Find( "AsteroidController" );
        Assert.IsNotNull( controllerObj , "Component not found" );
        
        asteroidController = controllerObj.GetComponent<AsteroidController>();

        asteroidController.OnAsteroidSpawn += OnAsteroidSpawn;
    }

    #endregion

    #region Handle Events

    void OnAsteroidSpawn( Asteroid a )
    {
        StartCoroutine( MaxReachableDistanceRoutine( a ) );
    }

    #endregion

    #region Private Methods 

    IEnumerator MaxReachableDistanceRoutine( Asteroid a )
    {
        Vector2     d                   = a.destination;
        float       asteroidSpeed       = a.speed;
        Vector2     turret              = (Vector2) gun.launchSpot.position;
        float       missileSpeed        = gun.missileSpeed;
        Vector2     collisionPoint      = Vector2.zero;
        float       delay               = 0;

        Vector2     o                   = IntersectionPointWithSkyPlane( a.origin , a.destination );
        float       timeToSkyline       = Vector2.Distance( a.origin , o ) / asteroidSpeed;
        Ray2D       ray                 = new Ray2D( o , ( d - o ).normalized );
        float       pathLength          = Vector2.Distance( d , o );

        yield return new WaitForSeconds( timeToSkyline );


        for( float distance = 0; distance < pathLength; distance += 0.01f )
        {
            collisionPoint = ray.GetPoint( distance );

            float asteroidCollisionTime     = distance / asteroidSpeed;
            float missileDistance           = Vector2.Distance( turret , collisionPoint );
            float missileCollisionTime      = missileDistance / missileSpeed; 

            delay = asteroidCollisionTime - missileCollisionTime;

            if( delay > 0 )
                break;
        }

        yield return new WaitForSeconds( delay );

        gun.ShootBeam( collisionPoint );
        gun.SetMarker( collisionPoint );

        yield return 0;
    }

    #endregion

    #region Private Methods

    Vector2 MinDistance( Vector2 P1 , Vector2 P2 , Vector2 P3 )
    {
        float u1 = (P3.x - P1.x)*(P2.x - P1.x) + (P3.y - P1.y)* (P2.y - P1.y );
        float u2 = (P2 - P1).sqrMagnitude;

        float u = u1 / u2;

        Vector2 closestPoint;

        if (u < 0) 
        {
            closestPoint = P1;
        } 
        else if (u > 1) 
        {
            closestPoint = P2;
        }
        else
        {
            closestPoint = new Vector2( P1.x + u * ( P2.x - P1.x ) , P1.y + u*( P2.y - P1.y ) );
        }

        return closestPoint;
    }

    Vector2 IntersectionPointWithSkyPlane( Vector2 P1 , Vector2 P2 )
    {
        Ray ray = new Ray( P1 , ( P2 - P1 ).normalized );
	    Plane skyPlane = new Plane( Vector3.down , new Vector3( 0 , 9.0f , 0 ) );

        float hit;
        if( skyPlane.Raycast( ray , out hit ) )
        {
           return ( Vector2 ) ray.GetPoint( hit );
        }
        else
        {
            Debug.LogWarning( "No collision with sky plane" );
            return P2;
        }
    }
    #endregion
}
