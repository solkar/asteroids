//
//
// GunController  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;

[RequireComponent( typeof( MissileLauncher ) )]
public class GunController : MonoBehaviour 
{

    #region Public Variables

    public int      ammo = 1;
    public int      roundSize = 1;

    #endregion

    #region Public Properties

    public bool     AmmoDepleted
    {
        get { return ammo <= 0; }
    }

    #endregion

    #region Private Variables

    private MissileLauncher  gun;

    #endregion

    #region MonoBehaviour Functions

    void Awake()
    {
        gun = GetComponent<MissileLauncher>();

        gun.OnShoot         += ConsumeAmmo;
        gun.OnMissileBlast  += OnMissileBlast;
    }

    void Start()
    {
        CheckGun();
    }
    
    #endregion

    #region Public Methods

    public void LoadRound()
    {
        ammo += roundSize;

        CheckGun();
    }

    public void ConsumeAmmo( int payload )
    {
        ammo -= payload;

        Assert.IsTrue( ammo >= 0 );
    
        CheckGun();
    }
    
    #endregion

    #region Private Methods

    void CheckGun()
    {
        if( AmmoDepleted )
        {
            gun.CanShoot = false;
        }
        else
        {
            gun.CanShoot = true;
        }
    }

    void OnMissileBlast()
    {
        LoadRound(); 
    }

    #endregion
}
