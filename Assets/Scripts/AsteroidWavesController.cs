﻿//
//
// AsteroidWavesController  
//
// Created by Karlos Zafra
// 
//

using UnityEngine;
using System.Collections;

public class AsteroidWavesController : MonoBehaviour 
{
    #region Public Variables

    public float                delayBetweenAsteroids = 1.0f;
    public AsteroidController   controller;
    public bool                 enableWaves = false;
    public int                  waveFrequency = 4;
    public int                  waveSize = 2;

    #endregion
    
    #region Private Variables

    private int                 counter = 0;

    #endregion

    #region MonoBehaviour Functions

    void Start()
    {
        controller.SpawnSolitaire();
        controller.OnAsteroidExplosion += OnAsteroidExplosion;

        counter++;
    }

    #endregion

    #region Private Methods
    
    void OnAsteroidExplosion()
    {
        counter++;

        if( enableWaves && counter == waveFrequency )
        {
            counter = 0;
            StartCoroutine( SpawnPack() );
        }
        else
        {

        StartCoroutine( SpawnSolitaire() );
        }

    }

    IEnumerator SpawnSolitaire()
    {
        yield return new WaitForSeconds( delayBetweenAsteroids );

        controller.SpawnSolitaire();
    }

    IEnumerator SpawnPack()
    {
        yield return new WaitForSeconds( delayBetweenAsteroids );

        controller.SpawnAsteroidPack( waveSize );
    }

    #endregion
}
